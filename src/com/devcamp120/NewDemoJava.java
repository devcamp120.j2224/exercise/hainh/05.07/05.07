package com.devcamp120;

public class NewDemoJava {
    public static void main(String[] args) {
        System.out.println("new demo java class");

        // ví dụ public static void

        NewDemoJava.name(30 , "nguyen van qua");


        // ví dụ về phương thức thường , phải tạo đối tượng rồi mới gọi dc
        NewDemoJava demo = new NewDemoJava();
        // ví dụ về public void 
        demo.name("nguyen van nam");
        // ví dụ về public string name 
        String strname = demo.name("nguyen van hai" , 32);
        System.out.println(strname);
    }

    // nếu xài public void thì ko cần return , nếu xài public String : name thì fai có return
    public void name(String strName){
        System.out.println(strName);
    }
    public String name(String strName , int age){
        String strResult = "My name is " + strName + " , my age is " + age;
        return strResult ;
    }
    // nếu là phương thức có static thì gọi trực tiếp dc
    public static void name(int age , String strName){
        String strResult = "My name is " + strName + " , my age is " + age;
        System.out.println(strResult);
    }
}

