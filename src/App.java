public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("hellow world");

        // vd sử dụng biến String
        String strname = new String("devcamp");

        System.out.println(strname); // commend 1 dòng : in biến String ra console 

        /* ví dụ sử dụng các phương thức của lớp String */

        System.out.println(" chuyển về chữ thường : toLowerCase " + strname.toLowerCase());
        System.out.println(" chuyển về chữ thường : toUpperCase " + strname.toUpperCase());
        System.out.println(" chiều dài của chuỗi : leng " + strname.length());
    }
}
/* commend nhiều dòng  , theo khối  */